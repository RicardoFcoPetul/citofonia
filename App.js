import React, { useEffect } from 'react';
import AppNavigation from './src/navigations/AppNavigation';
import { AppLoading } from 'expo';
import { useFonts } from 'expo-font';
import DropdownAlert from 'react-native-dropdownalert';
import DropDownHolder from './src/services/dropDownHolder';
import moment from 'moment';
import 'moment/min/locales';
import momentES from 'moment/src/locale/es';
import { StatusBar } from 'react-native';

import { Provider } from 'react-redux';
import store from './src/redux/store';
import { colors } from './src/constants';

export default function App() {
  useEffect(() => {
    moment.locale('es', momentES);
  }, []);

  let [fontsLoaded] = useFonts({
    'MADEFutureX-Light': require('./assets/fonts/MADE-Future-X-Light-PERSONAL-USE.otf'),
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return (
    <Provider store={store}>
      <StatusBar
        backgroundColor={colors.MAIN_BACKGROUND_COLOR}
        barStyle='dark-content'
      />
      <AppNavigation />
      <DropdownAlert
        ref={ref => {
          DropDownHolder.setDropDownRef(ref);
        }}
        showCancel
      />
    </Provider>
  );
}
