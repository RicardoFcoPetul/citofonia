//Common
export const WHITE_SMOKE = '#fefefe';
export const BLUE = '#3f64f1';
export const BLACK = '#000000';

//Nav
export const MAIN_BACKGROUND_COLOR = WHITE_SMOKE;
export const BOTTOM_TAB__BACKGROUND = '#f6f6f6';
export const INACTIVE_TAB_COLOR = '#5b5b5b';

export const GRAY = '#eeeeef';
export const GRAY_LIGHT = '#f5f5f5';
export const GRAY_EXTRA_LIGHT = '#fafafa';

export const DARK = '#1C1C1C';
export const DARK_MEDIUM = '#8a8a8d';
export const DARK_LIGHT = '#c6c6c8';
