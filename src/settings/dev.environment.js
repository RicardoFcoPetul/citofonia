export const Enviroment = {
  production: false,
  apiUrl: 'https://admon.com.co/index.php/apis/v2/',
  endpointUnauthorized: {
    contacts: 'getContacts',
    interior: 'getInterior',
    callLogs: 'getLogCalls',
  },
  endpointWithAuth: {},
};
