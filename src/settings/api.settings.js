import { Enviroment } from './dev.environment';

class ApiSettings {
  constructor() {
    this.API_URL = Enviroment.apiUrl;

    this.ENDPOINT_CONTACTS =
      this.API_URL + Enviroment.endpointUnauthorized.contacts;

    this.ENDPOINT_INTERIOR =
      this.API_URL + Enviroment.endpointUnauthorized.interior;

    this.ENDPOINT_CALL_LOGS =
      this.API_URL + Enviroment.endpointUnauthorized.callLogs;

    const obj = Enviroment.endpointUnauthorized;

    this.UNAUTHORIZED_PATHS = Object.keys(obj).map(k => obj[k]);
  }
}
export default new ApiSettings();
