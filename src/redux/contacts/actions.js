import * as actionCreators from './action-creators';

export const fetchContacts = () => async dispatch =>
  await dispatch(actionCreators.getContacts());
