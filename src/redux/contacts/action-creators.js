import { RSAA } from '../../constants';
import { apiSettings } from '../../settings';

import * as types from './types';
const RSAA_AUTH = RSAA.RSAA_AUTH;

export const getContacts = () => ({
  [RSAA_AUTH]: {
    method: 'GET',
    types: [
      types.CONTACTS_PENDING,
      types.CONTACTS_FULFILLED,
      types.CONTACTS_REJECTED,
    ],
    endpoint: apiSettings.ENDPOINT_CONTACTS,
  },
});

