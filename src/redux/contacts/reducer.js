import * as types from './types';

const intialState = {
  data: [],
  pending: false,
  error: false,
  message: '',
};

const reducer = (state = intialState, action) => {
  switch (action.type) {
    case types.CONTACTS_PENDING:
      return {
        ...state,
        pending: true,
        error: false,
        message: '',
      };
    case types.CONTACTS_FULFILLED:
      return {
        ...state,
        data: action.payload.result,
        pending: false,
        error: false,
        message: '',
      };
    case types.CONTACTS_REJECTED:
      return {
        ...state,
        pending: false,
        error: false,
        message: action.payload.message,
      };
    default:
      return state;
  }
};

export default reducer;
