import { RSAA } from '../../constants';
import { apiSettings } from '../../settings';

import * as types from './types';
const RSAA_AUTH = RSAA.RSAA_AUTH;

export const getInterns = () => ({
  [RSAA_AUTH]: {
    method: 'GET',
    types: [
      types.INTERNS_PENDING,
      types.INTERNS_FULFILLED,
      types.INTERNS_REJECTED,
    ],
    endpoint: apiSettings.ENDPOINT_INTERIOR,
  },
});

