import * as actionCreators from './action-creators';

export const fetchInterns = () => async dispatch =>
  dispatch(actionCreators.getInterns());

