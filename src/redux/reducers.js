import { combineReducers } from 'redux';

import contactsReducer from './contacts/reducer';
import callHistoryReducer from './callHistory/reducer';
import internsReducer from './interns/reducer';

export default combineReducers({
  contacts: contactsReducer,
  callHistory: callHistoryReducer,
  interns: internsReducer,
});
