import * as actionCreators from './action-creators';

export const fetchCallHistory = () => async dispatch =>
  dispatch(actionCreators.getCallHistory());
