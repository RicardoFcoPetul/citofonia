import { RSAA } from '../../constants';
import { apiSettings } from '../../settings';

import * as types from './types';
const RSAA_AUTH = RSAA.RSAA_AUTH;

export const getCallHistory = () => ({
  [RSAA_AUTH]: {
    method: 'GET',
    types: [
      types.GET_CALL_HISTORY_PENDING,
      types.GET_CALL_HISTORY_FULFILLED,
      types.GET_CALL_HISTORY_REJECTED,
    ],
    endpoint: apiSettings.ENDPOINT_CALL_LOGS,
  },
});
