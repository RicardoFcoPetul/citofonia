import * as types from './types';

const intialState = {
  data: [],
  pending: false,
  error: false,
  message: '',
};

const reducer = (state = intialState, action) => {
  switch (action.type) {
    case types.GET_CALL_HISTORY_PENDING:
      return {
        ...state,
        pending: true,
        error: false,
        message: '',
      };
    case types.GET_CALL_HISTORY_FULFILLED:
      return {
        ...state,
        data: action.payload.result,
        pending: false,
        error: false,
        message: '',
      };
    case types.GET_CALL_HISTORY_REJECTED:
      return {
        ...state,
        pending: false,
        error: false,
        message: action.payload.message,
      };
    default:
      return state;
  }
};

export default reducer;
