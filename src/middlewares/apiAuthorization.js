import { RSAA as RSAAConstants } from '../constants';
import { apiSettings } from '../settings';
import { toQueryString } from '../utils';
import axios from 'axios';
import _ from 'lodash';
import { RSAA } from 'redux-api-middleware';
export const RSAA_AUTH = RSAAConstants.RSAA_AUTH;

const apiAuthorization = store => next => action => {
  if (!action[RSAA_AUTH]) {
    return next(action);
  }

  const state = store.getState();
  const { [RSAA_AUTH]: request } = action;

  request.headers = {
    ...request.headers,
  };
  const path = request.endpoint.split(apiSettings.API_URL)[1];
  const finalPath = path.split('?')[0];
  if (!apiSettings.UNAUTHORIZED_PATHS.includes(finalPath)) {
    // request.headers.Authorization = ``;
  }
  if (_.has(action[RSAA_AUTH], 'query')) {
    request.endpoint = [
      request.endpoint.replace(/\?*/, ''),
      toQueryString(request.query),
    ].join('?');
    delete request.query;
  }

  request.fetch = async (url, params) => {
    const AxiosRequestConfig = {
      url,
      ...params,
      validateStatus() {
        return true;
      },
      timeout: 10000,
    };

    if (params.body) {
      AxiosRequestConfig.data = params.body;
    }
    const axiosResponse = await axios(AxiosRequestConfig);
    axiosResponse.ok = true;

    if (axiosResponse.status !== 200) {
      axiosResponse.ok = false;
      axiosResponse.statusText = _.get(axiosResponse, 'data.error.message', '');
      if (axiosResponse.status === 400) {
        // axiosResponse.statusText = ""
      }
    }

    if (axiosResponse.status === 401) {
      //redict to login
    }
    const response = { result: axiosResponse.data };
    axiosResponse.data = response;

    return new window.Response(
      JSON.stringify({
        ...axiosResponse.data,
      }),
      {
        status: axiosResponse.status,
        statusText: axiosResponse.statusText,
        headers: axiosResponse.headers,
      }
    );
  };

  action[RSAA] = request;
  return next(action);
};

export default apiAuthorization;
