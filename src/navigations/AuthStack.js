import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/Auth/Login';
import { screenNames } from '../constants';

const Stack = createStackNavigator();

export default function AuthStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={screenNames.LOGIN_SCREEN}
        component={Login}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
