import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AuthStack from './AuthStack';
import CustomerStack from './CustomerStack';
import { screenNames } from '../constants';

const Stack = createStackNavigator();

const AppNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={screenNames.AUTH_STACK}>
        <Stack.Screen
          name={screenNames.AUTH_STACK}
          component={AuthStack}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={screenNames.CUSTOMER_STACK}
          component={CustomerStack}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
