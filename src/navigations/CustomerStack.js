import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

import CallHistory from '../screens/Customer/CallHistory';
import Interns from '../screens/Customer/Interns';
import Residents from '../screens/Customer/Residents';

import { colors, screenNames } from '../constants';

const Tab = createBottomTabNavigator();

export default function CustomerStack() {
  return (
    <Tab.Navigator
      initialRouteName={screenNames.RESIDENTS_SCREEN}
      tabBarOptions={{
        inactiveTintColor: colors.INACTIVE_TAB_COLOR,
        activeTintColor: colors.BLACK,
        style: { backgroundColor: colors.BOTTOM_TAB__BACKGROUND },
      }}
    >
      <Tab.Screen
        name={screenNames.INTERNS_SCREEN}
        component={Interns}
        options={{
          title: 'Internos',
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name='suitcase' color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name={screenNames.RESIDENTS_SCREEN}
        component={Residents}
        options={{
          title: 'Residentes',
          tabBarIcon: ({ color, size }) => (
            <Ionicons name='ios-people' color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name={screenNames.CALL_HISTORY}
        component={CallHistory}
        options={{
          title: 'Historial',
          tabBarIcon: ({ color, size }) => (
            <MaterialIcons name='compare-arrows' color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
