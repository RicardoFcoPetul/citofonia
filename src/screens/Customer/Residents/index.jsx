import React, { useEffect, useState } from 'react';
import { Container, Header, Contact, Loading, Text } from '../../../components';
import { FlatList, View, Platform, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import * as Contacts from 'expo-contacts';
import call from 'react-native-phone-call';

import { fetchContacts } from '../../../redux/contacts/actions';

const Residents = () => {
  const listContactsAPI = useSelector(state => state.contacts.data);
  const isPending = useSelector(state => state.contacts.pending);

  const [residents, setResidents] = useState([]);
  const [localCompanyContacts, setLocalCompanyContacts] = useState([]);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    requestPermissions();
  }, []);

  useEffect(() => {
    if (listContactsAPI.length > 0) {
      compareContacts();
      setResidents(listContactsAPI);
    }
  }, [listContactsAPI]);

  const requestPermissions = async () => {
    const { status } = await Contacts.requestPermissionsAsync();
    if (status === 'granted') {
      const { data } = await Contacts.getContactsAsync();
      dispatch(fetchContacts());
      getLocalContactsByCompany(data);
    }
  };

  const getLocalContactsByCompany = contactListLocal => {
    if (contactListLocal.length > 0) {
      const companyContacts = contactListLocal.filter(
        contact => contact.company === 'Citofonia'
      );
      setLocalCompanyContacts(companyContacts);
    }
  };

  const compareContacts = () => {
    const contactsListWithPhoneNumber = listContactsAPI.filter(
      contact => contact.telefono
    );
    let contactsToWrite = [];
    if (localCompanyContacts.length > 0) {
      for (let i = 0; i < localCompanyContacts.length; i++) {
        for (let j = 0; j < contactsListWithPhoneNumber.length; j++) {
          let exist = false;
          const localContact = localCompanyContacts[i];
          const contactAPI = contactsListWithPhoneNumber[j];
          if (localContact.department === contactAPI.idusuario) {
            exist = true;
            contactsListWithPhoneNumber.splice(j, 1);
            break;
          }
          if (j === contactsListWithPhoneNumber.length - 1 && exist === false) {
            contactsToWrite.push(contactAPI);
          }
        }
      }
    }
    contactsToWrite = [...contactsToWrite, ...contactsListWithPhoneNumber];

    createContacts(contactsToWrite);
  };

  const createContacts = contactsToWrite => {
    contactsToWrite.forEach(async contact => {
      const { nombre, correo, telefono, idusuario } = contact;
      const fullName = nombre.split('-');
      const firstName = fullName[0];
      const lastName = fullName[1];
      const emails = {
        email: correo,
        isPrimary: true,
        label: 'home',
      };

      const phoneNumber = {
        isPrimary: true,
        label: 'home',
        number: parsePhoneNumber(telefono),
        digits: telefono,
      };

      const newContact = {
        [Contacts.Fields.FirstName]: firstName,
        [Contacts.Fields.LastName]: lastName,
        [Contacts.Fields.Emails]: [emails],
        [Contacts.Fields.PhoneNumbers]: [phoneNumber],
        [Contacts.Fields.Company]: 'Citofonia',
        [Contacts.Fields.Department]: idusuario,
      };
      try {
        if (Platform.OS === 'android') {
          await Contacts.addContactAsync(newContact);
        } else {
          const containerId = await Contacts.getDefaultContainerIdAsync();
          await Contacts.addContactAsync(newContact, containerId);
        }
      } catch (error) {
        Alert.alert(
          'Error',
          'Ocurrio un error y no pudimos sincronizar los contactos',
          [{ text: 'OK' }],
          { cancelable: false }
        );
      }
    });
  };
  const parsePhoneNumber = number => {
    if (number.length !== 10) return number;

    const first = number.slice(0, 3);
    const second = number.slice(3, 6);
    const third = number.slice(6, 10);
    return `${first} ${second} ${third}`;
  };
  const makeCall = ({ telefono }) => {
    if (!telefono) {
      return Alert.alert(
        'Error',
        'Este contacto no tiene un número disponible',
        [{ text: 'OK' }],
        { cancelable: false }
      );
    }
    const args = {
      number: telefono,
      prompt: false,
    };

    call(args).catch(console.error);
  };

  const refreshContacts = async () => {
    setIsRefreshing(true);
    dispatch(fetchContacts());
    setIsRefreshing(false);
  };

  const handleSearch = searchTerm => {
    if (!searchTerm) return setResidents(listContactsAPI);
    const coincidences = listContactsAPI.filter(contact =>
      contact.nombre.toLowerCase().match(searchTerm.toLowerCase())
    );
    setResidents(coincidences);
  };

  if (isPending) return <Loading />;
  return (
    <Container safe>
      <Header handleSearch={handleSearch} />
      <FlatList
        data={residents}
        extraData={residents}
        onRefresh={refreshContacts}
        refreshing={isRefreshing}
        renderItem={({ item }) => (
          <Contact item={item} onPress={() => makeCall(item)} />
        )}
        keyExtractor={(item, index) => `contact_${index}`}
        ListEmptyComponent={<Text text='No hay contactos' />}
        ListFooterComponent={<View style={{ height: 200 }} />}
      />
    </Container>
  );
};

export default Residents;
