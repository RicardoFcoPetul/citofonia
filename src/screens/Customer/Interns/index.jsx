import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Header, Contact, Loading, Text } from '../../../components';
import { FlatList, View, Alert } from 'react-native';
import call from 'react-native-phone-call';

import { fetchInterns } from '../../../redux/interns/actions';

const Interns = () => {
  const listInterns = useSelector(state => state.interns.data);
  const isPending = useSelector(state => state.interns.pending);

  const [interns, setInterns] = useState([]);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchInterns());
  }, []);

  useEffect(() => {
    setInterns(listInterns);
  }, [listInterns]);

  const makeCall = ({ telefono }) => {
    if (!telefono) {
      return Alert.alert(
        'Error',
        'Este contacto no tiene un número disponible',
        [{ text: 'OK' }],
        { cancelable: false }
      );
    }
    const args = {
      number: telefono,
      prompt: false,
    };

    call(args).catch(console.error);
  };

  const refreshInterns = async () => {
    setIsRefreshing(true);
    dispatch(fetchInterns());
    setIsRefreshing(false);
  };

  const handleSearch = searchTerm => {
    if (!searchTerm) return setInterns(listInterns);
    const coincidences = listInterns.filter(internt =>
      internt.nombre.toLowerCase().match(searchTerm.toLowerCase())
    );
    setInterns(coincidences);
  };

  if (isPending) return <Loading />;

  return (
    <Container safe>
      <Header handleSearch={handleSearch} />
      <FlatList
        data={interns}
        extraData={interns}
        onRefresh={refreshInterns}
        refreshing={isRefreshing}
        renderItem={({ item }) => (
          <Contact item={item} type='interns' onPress={() => makeCall(item)} />
        )}
        keyExtractor={(item, index) => `interns_${index}`}
        ListEmptyComponent={<Text text='No hay contactos internos' />}
        ListFooterComponent={<View style={{ height: 180 }} />}
      />
    </Container>
  );
};

export default Interns;
