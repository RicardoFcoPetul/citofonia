import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Header, Contact, Loading, Text } from '../../../components';
import { FlatList, View } from 'react-native';

import { fetchCallHistory } from '../../../redux/callHistory/actions';

const CallHistory = () => {
  const callHistory = useSelector(state => state.callHistory.data);
  const isPending = useSelector(state => state.callHistory.pending);

  const [callLogs, setCallLogs] = useState([]);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCallHistory());
  }, []);

  useEffect(() => {
    setCallLogs(callHistory);
  }, [callHistory]);

  const refreshHistory = async () => {
    setIsRefreshing(true);
    dispatch(fetchCallHistory());
    setIsRefreshing(false);
  };

  const handleSearch = searchTerm => {
    if (!searchTerm) return setCallLogs(callHistory);
    const coincidences = callHistory.filter(call => {
      if (
        call.textoemisor.toLowerCase().match(searchTerm.toLowerCase()) ||
        call.destino.toLowerCase().match(searchTerm.toLowerCase())
      ) {
        return call;
      }
    });
    setCallLogs(coincidences);
  };

  if (isPending) return <Loading />;

  return (
    <Container safe>
      <Header handleSearch={handleSearch} />
      <FlatList
        data={callLogs}
        extraData={callLogs}
        onRefresh={refreshHistory}
        refreshing={isRefreshing}
        renderItem={({ item }) => <Contact item={item} type='history' />}
        keyExtractor={(item, index) => `call_history_${index}`}
        ListEmptyComponent={<Text text='No hay registro de llamadas' />}
        ListFooterComponent={<View style={{ height: 180 }} />}
      />
    </Container>
  );
};

export default CallHistory;
