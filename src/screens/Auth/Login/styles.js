import { StyleSheet } from 'react-native';
import { colors } from '../../../constants';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.GRAY_LIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  centerContent: {
    width: '85%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
  },
  buttonContainer: {
    width: '100%',
  },
  buttonStyle: {
    backgroundColor: colors.BLUE,
    paddingVertical: 15,
  },
  textForgotPassword: {
    marginTop: 15,
  },
});

export default styles;
