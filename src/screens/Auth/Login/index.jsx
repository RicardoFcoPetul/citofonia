import React, { useState } from 'react';
import {
  Image,
  KeyboardAvoidingView,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
import { TextInput, Text } from '../../../components';
import { imagesGlobal, screenNames } from '../../../constants';
import { Button } from 'react-native-elements';
import DropDownHolder from '../../../services/dropDownHolder';

import styles from './styles';

const Login = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const forgotPassword = () =>
    DropDownHolder.showAlert('Comunicate con el administrador de tu conjunto', '', 'warn');

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={styles.container}
    >
      <SafeAreaView style={styles.centerContent}>
        <Image source={imagesGlobal.LOGIN_IMAGE} style={styles.logo} />
        <TextInput
          label='Correo'
          placeholder='Escribe tu correo'
          onChangeText={text => setEmail(text)}
          value={email}
        />
        <TextInput
          label='Contraseña'
          placeholder='Escribe tu contraseña'
          secureTextEntry
          onChangeText={text => setPassword(text)}
          value={password}
        />
        <Button
          title='Ingresar'
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.buttonStyle}
          onPress={() => navigation.navigate(screenNames.CUSTOMER_STACK)}
        />
        <TouchableWithoutFeedback onPress={forgotPassword}>
          <Text style={styles.textForgotPassword} text='¿Olvidaste tu constraseña?' />
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default Login;
