class DropDownHolder {
  static dropDownRef;

  static setDropDownRef(dropDownRef) {
    if (dropDownRef) {
      DropDownHolder.dropDownRef = dropDownRef;
    }
  }

  static getDropDownRef() {
    return DropDownHolder.dropDownRef;
  }

  static showAlert(message, title = '', type) {
    DropDownHolder.getDropDownRef().alertWithType(
      type || 'info',
      title,
      message
    );
  }

  static showErrorAlert(message, title = 'Error') {
    DropDownHolder.getDropDownRef().alertWithType('error', title, message);
  }
}

export default DropDownHolder;
