function toQueryString(json) {
  return Object.keys(json)
    .map(function (key) {
      let objectType;
      if (typeof json[key] === 'object') {
        objectType = JSON.stringify(json[key]);
      } else {
        objectType = encodeURIComponent(json[key]);
      }
      const r = encodeURIComponent(key) + '=' + objectType;
      return r;
    })
    .join('&');
}

export { toQueryString };
