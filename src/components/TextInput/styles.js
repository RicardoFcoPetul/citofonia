import { StyleSheet } from 'react-native';
import { colors } from '../../constants';

const styles = StyleSheet.create({
  containerStyle: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  inputContainerStyle: {
    backgroundColor: colors.GRAY_EXTRA_LIGHT,
    width: '100%',
    borderBottomWidth: 0,
    borderRadius: 5,
    shadowColor: colors.BLACK,
    paddingHorizontal: 5,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  labelStyle: {
    color: colors.DARK,
  },
});

export default styles;
