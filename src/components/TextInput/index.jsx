import React from 'react';
import { Input } from 'react-native-elements';
import PropTypes from 'prop-types';

import styles from './styles';

const TextInput = ({ ...props }) => {
  return (
    <Input
      {...props}
      labelStyle={styles.labelStyle}
      containerStyle={styles.containerStyle}
      inputContainerStyle={styles.inputContainerStyle}
      onChangeText={props.onChangeText}
    />
  );
};

TextInput.propTypes = {
  onChangeText: PropTypes.func,
};

export default TextInput;
