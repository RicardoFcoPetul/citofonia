import Container from './Container';
import TextInput from './TextInput';
import Text from './Text';
import Header from './Header';
import Contact from './Contact';
import Loading from './Loading';

export { Container, TextInput, Text, Header, Contact, Loading };
