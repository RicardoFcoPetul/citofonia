import React from 'react';
import { StyleSheet, ActivityIndicator, View } from 'react-native';
import { colors } from '../../constants';
import Text from '../Text';
import PropTypes from 'prop-types';

const Loading = ({ message }) => {
  return (
    <View style={styles.contain}>
      <ActivityIndicator size='large' />
      {message && <Text text={message} />}
    </View>
  );
};

Loading.propTypes = {
  message: PropTypes.string,
};

export default Loading;

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.WHITE_SMOKE,
  },
});
