import React, { useEffect, useState } from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import { imagesGlobal } from '../../constants';
import Text from '../Text';
import moment from 'moment';

import styles from './styles';

const Contact = ({ item, onPress, type }) => {
  const [apartment, setApartment] = useState('');
  const [userName, setUserName] = useState('');
  useEffect(() => {
    parseData();
  }, [item]);

  const parseData = () => {
    let fullName = [];
    if (item.nombre) {
      fullName = item.nombre.split('-');
      setApartment(fullName[0]);
      setUserName(fullName[1]);
    }
    if (type === 'history') {
      if (item.tipo === '1') {
        fullName = item.destino.split('-');
        setApartment(fullName[0]);
        setUserName(fullName[1]);
      } else {
        fullName = item.textoemisor.split('-');
        setApartment(fullName[0]);
        setUserName(fullName[1]);
      }
    }
    if (type === 'interns') {
      setApartment(item.nombre);
      setUserName('');
    }
  };

  const renderIcon = () => {
    if (type === 'history') {
      if (item.tipo === '1') {
        return (
          <Image
            source={imagesGlobal.CALL_ICON}
            style={[styles.callIcon, styles.outgoingCall]}
          />
        );
      } else {
        return (
          <Image source={imagesGlobal.CALL_ICON} style={styles.callIcon} />
        );
      }
    }
  };

  const renderDate = () => {
    if (type == 'history')
      return (
        <Text
          text={moment(item.fecha, 'YYYYMMDD').format('L')}
          style={[styles.text, styles.date]}
        />
      );
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <View style={styles.mainContent}>
          <View style={styles.leftContent}>
            {renderIcon()}
            <View>
              <Text text={apartment} type='subtitle' />
              <Text text={userName} style={styles.text} />
            </View>
          </View>
          {renderDate()}
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Contact;
