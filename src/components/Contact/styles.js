import { StyleSheet } from 'react-native';
import { colors, spaces } from '../../constants';

const styles = StyleSheet.create({
  container: {
    maxWidth: '100%',
    borderBottomWidth: 1,
    borderBottomColor: colors.DARK_LIGHT,
    paddingVertical: 5,
    paddingHorizontal: spaces.PADDING_HORIZONTAL,
  },
  mainContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftContent: {
    flexDirection: 'row',
    alignItems: 'center',
    maxWidth: '66%',
  },
  callIcon: {
    width: 30,
    height: 30,
  },
  outgoingCall: {
    transform: [{ rotate: '180deg' }],
  },
  text: {
    color: colors.DARK_MEDIUM,
    fontSize: 14,
  },
  date: {
    maxWidth: '30%',
    textAlign: 'right',
  },
});

export default styles;
