import { StyleSheet } from 'react-native';
import { colors, spaces } from '../../constants';

const styles = StyleSheet.create({
  headerContainer: {
    marginTop: 15,
    paddingHorizontal: spaces.PADDING_HORIZONTAL,
  },
  containerStyle: {
    backgroundColor: colors.MAIN_BACKGROUND_COLOR,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
  inputContainerStyle: {
    borderRadius: 15,
    backgroundColor: colors.GRAY,
    height: 35,
  },
});

export default styles;
