import React, { useState } from 'react';
import { View } from 'react-native';
import { SearchBar } from 'react-native-elements';
import Text from '../Text';

import styles from './styles';

const Header = ({ handleSearch }) => {
  const [search, setSearch] = useState('');

  const updateSearch = search => {
    setSearch(search);
    handleSearch(search);
  };

  return (
    <View style={styles.headerContainer}>
      <Text type='title' text='Admon Citofonia' />
      <SearchBar
        placeholder='Search'
        lightTheme
        value={search}
        onChangeText={updateSearch}
        containerStyle={styles.containerStyle}
        inputContainerStyle={styles.inputContainerStyle}
        inputStyle={styles.inputStyle}
      />
    </View>
  );
};

export default Header;
