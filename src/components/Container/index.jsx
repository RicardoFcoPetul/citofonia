import React from 'react';
import {
  View,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const Container = ({ children, style, safe }) => {
  const dissmissKeyboard = () => {
    Keyboard.dismiss();
  };

  if (safe)
    return (
      <SafeAreaView>
        <TouchableWithoutFeedback onPress={dissmissKeyboard}>
          <View style={[styles.container, style]}>{children}</View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  return (
    <TouchableWithoutFeedback onPress={dissmissKeyboard}>
      <View style={styles.container}>{children}</View>
    </TouchableWithoutFeedback>
  );
};

Container.propTypes = {
  children: PropTypes.node,
  safe: PropTypes.bool,
  backgroundColor: PropTypes.string,
};

export default Container;
