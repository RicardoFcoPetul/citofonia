import { StyleSheet } from 'react-native';
import { colors } from '../../constants';
import { Dimensions } from 'react-native';

const HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.MAIN_BACKGROUND_COLOR,
    minHeight: HEIGHT,
  },
});

export default styles;
