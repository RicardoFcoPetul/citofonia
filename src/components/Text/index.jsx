import React from 'react';
import { Text as RNText } from 'react-native';

import styles from './styles';

const Text = ({ text, ...props }) => {
  let textStyles = {};
  if (props.type === 'title') textStyles = styles.title;
  if (props.type === 'subtitle') textStyles = styles.subtitle;

  return (
    <RNText {...props} style={[styles.text, textStyles, props.style]}>
      {text}
    </RNText>
  );
};

export default Text;
