import { StyleSheet } from 'react-native';
import { colors } from '../../constants';

const styles = StyleSheet.create({
  text: {
    fontFamily: 'MADEFutureX-Light',
    fontWeight: 'bold',
    color: colors.DARK,
  },
  title: {
    fontSize: 24,
  },
  subtitle: {
    color: colors.BLACK,
    fontSize: 18,
  },
});

export default styles;
